Shiny server
====================

This project holds the data found in `/srv/shiny-server` on the `shiny.math.uwaterloo.ca` server.

This git repository is served at `https://shiny.math.uwaterloo.ca`.

When new code is pushed it can take 30minutes for it to show up on the website.

Shiny logs can be found at: 'https://shiny.math.uwaterloo.ca/logs/'


Adding new shiny apps
---------------------

__DONT STORE PRIVATE DATA ON THIS SERVER!!__

__Take care to set limits on user input!__

Always ensure a user cant cause the server to crash or slow down solving large computations. Aim for everything to run snappy. This system is shared with students for class work so lets keep their experiance positive.

Store your apps in sub directories to keep the collections namespaced.

See: `/test` for examples of shiny apps. Please try and keep the file layout clean.

Apps can be accessed from a URL like: `https://shiny.math.uwaterloo.ca/test/sample-apps/hello`

The homepage `https://shiny.math.uwaterloo.ca` can also be edited from `index.html` You might also be able to create `index.html` at sub folders if you want something more theamed to your material.

Adding new R libs or system packages
------------------------------------

Please take care here. This is a shared system and some packages could perhaps break others shiny apps.

Reach out to s8weber@uwaterloo.ca to makes the update to the `requirements.sh` file.


Management (System Admins)
==========================

General system configuration is managed by MFCF in a central Salt repository.

This is a Salt-managed server, so as root you can run `salt-call state.highstate` to ensure state.
Highstate will automate some tasks like git pull using the shiny user and run `requirements.sh`, restart the `shiny-server`.

Admins can also view logs and other system file.

important files:
 - config: /etc/shiny-server
 - files: /srv/shiny-server
 - logs: /var/log/shiny-server*


missing log files?
```
shiny-server auto deletes logs if the app seems to run without error...
to workaround this; try running the following while the webapp seems to be loading:
tail -f /var/log/shiny-server/clinton-shiny-*
more info: https://github.com/rstudio/shiny-server/issues/82
```


requirements.sh
---------------

If you add a shiny app that needs extra R libs you can add something like the
following to `requirements.sh`.

```
# my-app required packages
<<EOF R -q --vanilla &> /tmp/rlog
need_pkg <- c('tidyverse', 'plotly', 'DT', 'devtools', 'Rfast', 'magrittr', 'shinydashboard', 'RPostgres')
missing_pkg <- need_pkg[!(need_pkg %in% installed.packages()[,"Package"])]
if(length(missing_pkg)) install.packages(missing_pkg, repos='http://cran.utstat.utoronto.ca/')
EOF
cat /tmp/rlog
grep -q 'non-zero exit status' /tmp/rlog && exit 9 || true
```

Once the `requirements.sh` is updated the shiny servers will need to be refreshed perhaps by a system maintainer

See: `https://shiny.math.uwaterloo.ca/logs` to view install logs.


Extra resources
----------------

more help
 - https://github.com/rstudio/shiny-server/blob/master/README.md
