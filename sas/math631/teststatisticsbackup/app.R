
### Likelihood intervals and principles of repeated sampling

library(shiny)

shinyApp(
  ui <- fluidPage(
    titlePanel("Exploring test Statistics"),
    sidebarLayout(
      sidebarPanel(#"Choose a distribution, specify the null hypothesis, and see how your test statistic and p-value changes as the point estimate changes.",

        wellPanel(
          # radio buttons for distribution
          radioButtons("dist", "Select distribution:",
                       c("Gaussian (\u03BC, \u03C3)" = "norm",
                         "Binomial (n, \u03B8)" = "binom",
                         "Poisson (\u03B8)" = "pois",
                         "Exponential (\u03B8)" = "exp"), inline = F),

          # radio buttons for method
          conditionalPanel(
            condition = "input.dist != 'norm'",

          conditionalPanel(
            condition = "input.dist != 'exp'",
          radioButtons("method", "Select test statistic:",
                       c("Asymptotic Gaussian test statistic" = "gauss",
                         "Likelihood ratio test statistic" = "lr"), inline = F),
         ),

          conditionalPanel(
            condition = "input.dist == 'exp'",
          radioButtons("expmethod", "Select test statistic:",
                       c("Asymptotic Gaussian test statistic" = "expgauss",
                         "Likelihood ratio test statistic" = "explr",
                         "Exact test statistic:" = "expexact"), inline = F),
         ),


         ),

          # Gaussian
          conditionalPanel(
            condition = "input.dist == 'norm'",
          # test mu or sigma?
               radioButtons("gtest", "Test for mean or standard deviation?",
                       c("Mean (\u03BC)" = "mutest",
                         "Standard deviation (\u03C3)" = "sigtest"), inline = F),        


          conditionalPanel(
            condition = "input.gtest == 'mutest'",
          # numeric input for mu0
            numericInput(inputId = "mu0",
                         label = "Enter H\u2080 value for \u03BC",
                         value = 0, min = -10, max = 10),

          # numeric input for sample size:
          numericInput(inputId = "nnormmu",
                       label = "Enter sample size",
                       value = 50, min = 5, max = 1000, step = 1),

# end of mutest conditional panel
          ),

          conditionalPanel(
            condition = "input.gtest == 'sigtest'",
          # numeric input for sig0
            numericInput(inputId = "sig0",
                         label = "Enter H\u2080 value for \u03C3",
                         value = 1, min = 0, max = 10),

          # numeric input for sample size:
          numericInput(inputId = "nnormsig",
                       label = "Enter sample size",
                       value = 50, min = 5, max = 1000, step = 1),

# end of sigtest conditional panel
          ),


          # slider for mean
          sliderInput(inputId = "ybar", 
                      "Sample mean:",
                      min = -10, max = 10, step = 0.1, value = 0.5),

          # slider for sd
          sliderInput(inputId = "sd", 
                      "Sample standard deviation:",
                      min = 0.1, max = 10, step = 0.1, value = 0.5),


# end of Gaussian conditional panel  
 ),


          # binomial
          conditionalPanel(
            condition = "input.dist == 'binom'",
          # numeric input for theta0
            numericInput(inputId = "theta0binom",
                         label = "Enter H\u2080 value for \u03B8",
                         value = 0.5, min = 0, max = 1),

          # numeric input for sample size:
          numericInput(inputId = "nbinom",
                       label = "Enter sample size",
                       value = 50, min = 5, max = 1000, step = 1),

          # slider for thetahat
          sliderInput(inputId = "thetahatbinom", 
                      "MLE of \u03B8:",
                      min = 0, max = 1, step = 0.05, value = 0.6),
          ),

          # Poisson
          conditionalPanel(
            condition = "input.dist == 'pois'",
          # numeric input for theta0
            numericInput(inputId = "theta0pois",
                         label = "Enter H\u2080 value for \u03B8",
                         value = 3, min = 0.1, max = 10),

          # numeric input for sample size:
          numericInput(inputId = "npois",
                       label = "Enter sample size",
                       value = 50, min = 5, max = 1000, step = 1),

          # slider for thetahat
          sliderInput(inputId = "thetahatpois", 
                      "MLE of \u03B8:",
                      min = 0.1, max = 10, step = 0.1, value = 3.5),
          ),

         # Exponential
          conditionalPanel(
            condition = "input.dist == 'exp'",
          # numeric input for theta0
            numericInput(inputId = "theta0exp",
                         label = "Enter H\u2080 value for \u03B8",
                         value = 2, min = 0.1, max = 10),

          # numeric input for sample size:
          numericInput(inputId = "nexp",
                       label = "Enter sample size",
                       value = 50, min = 5, max = 1000, step = 1),

          # slider for thetahat
          sliderInput(inputId = "thetahatexp", 
                      "MLE of \u03B8:",
                      min = 0.1, max = 10, step = 0.1, value = 2.5),
          ),

       # end of wellPanel 
      ),
      # end of sidebarPanel
    ),

      # create main panel for outputs
      mainPanel(
        # output plots
        plotOutput("HTPlot"),
      # end of mainPanel
      )
    # end of sidebarLayout
    )
  # end of ui
  ),

  server <- function(input, output) {

    output$HTPlot <- renderPlot({
      # get inputs
     if (input$dist != "norm") {
      that <- switch(input$dist,
                     binom = input$thetahatbinom,
                     pois = input$thetahatpois,
                     exp = input$thetahatexp)
      t0 <- switch(input$dist, 
                   binom = input$theta0binom,
                   pois = input$theta0pois,
                   exp = input$theta0exp)
      n <- switch(input$dist,
                  binom = input$nbinom,
                  pois = input$npois,
                  exp = input$nexp)
     } else {
         ybar <- input$ybar
         sd <- input$sd
       if (input$gtest == "mutest") {
          mu0 <- input$mu0
          n <- input$nnormmu
       } else {
         sig0 <- input$sig0
         n <- input$nnormsig
       }
     }

     # plot Gaussian hypothesis tests
    if (input$dist == 'norm') {
     # if testing mu
     if (input$gtest == 'mutest') {

if (input$nnormmu <= 10000) {

        # get test statistic value
        teststat <- abs(mu0 - ybar)/(sd/sqrt(n))  
        pvalue <- 2*(1 - pnorm(teststat))
         x.max <- 4
         x.min <- -x.max

        plot(0, 0, type = "n", xlim = c(x.min, x.max), ylim = c(0, 0.4), xlab = NA, ylab = NA, main = "G(0,1) Distribution", cex.axis = 2, yaxt = "n", cex.main = 2)
        curve(dnorm(x), col = 1, add = TRUE, lwd = 1.5)

text(-3, 0.35, bquote(mu[0] == .(mu0)), cex = 1.5)
text(-3, 0.3,bquote(bar(y) == .(ybar)), cex = 1.5)

text(2.5, 0.35, paste("Test statistic =", round(teststat, 3)), cex = 1.5)
if (pvalue >= 0.0001) {
  text(2.5, 0.3, paste("p-value =", round(pvalue, 4)), cex = 1.5)
} else {
  text(2.5, 0.3, paste("p-value < 0.0001"), cex = 1.5)
}

# plot interior
cord.x <- c(-teststat, seq(-teststat, teststat, 0.01), teststat)
cord.y <- c(0, dnorm(seq(-teststat, teststat, 0.01)), 0)
polygon(cord.x,cord.y,col='light blue')
polygon(cord.x,cord.y,col='navy', density = 3, lwd = 2)

# plot tails
if (teststat < 4) {
cord.x <- c(teststat, seq(teststat, x.max, 0.01), x.max)
cord.y <- c(0, dnorm(seq(teststat, x.max, 0.01)), 0)
polygon(cord.x,cord.y,col='lightpink')
polygon(cord.x,cord.y,col='indianred3', density = 7, angle = 135, lwd = 3)
cord.x <- c(x.min, seq(x.min, -teststat, 0.01), -teststat)
cord.y <- c(0, dnorm(seq(x.min, -teststat, 0.01)), 0)
polygon(cord.x,cord.y,col='lightpink')
polygon(cord.x,cord.y,col='indianred3', density = 7, angle = 135, lwd = 3)
}

}
# end of mutest plot
    }


     # if testing sigma
    if (input$gtest == 'sigtest') {
if (input$nnormsig <= 10000) {
        # get test statistic value
        teststat <- (n-1)*sd*sd/(sig0*sig0)
         plow <- pchisq(teststat, n - 1)
         phigh <- 1 - pchisq(teststat, n - 1)
         pvalue <- as.numeric(plow < phigh)*2*plow + as.numeric(plow >= phigh)*2*phigh
         x.max <- 2*n
         x.min <- 0

        plot(0, 0, type = "n", xlim = c(x.min, x.max), ylim = c(0, max(dchisq(n - 1, n - 1))), xlab = NA, ylab = NA, main = expression(paste(chi^2, "Distribution")), cex.axis = 2, yaxt = "n", cex.main = 2)
        curve(dchisq(x, n - 1), col = 1, add = TRUE, lwd = 1.5)

text(0.1*(n - 1), 0.8*dchisq(n -1 , n - 1), bquote(sigma[0] == .(sig0)), cex = 1.5)
text(0.1*(n - 1), 0.6*dchisq(n -1 , n - 1),bquote(s == .(sd)), cex = 1.5)

text(1.7*(n - 1), 0.8*dchisq(n -1 , n - 1), paste("Test statistic =", round(teststat, 3)), cex = 1.5)
if (pvalue >= 0.0001) {
  text(1.7*(n - 1), 0.6*dchisq(n -1 , n - 1), paste("p-value =", round(pvalue, 4)), cex = 1.5)
} else {
  text(1.7*(n - 1), 0.6*dchisq(n -1 , n - 1), paste("p-value < 0.0001"), cex = 1.5)
}

# plot if low value
if (plow < phigh) {
cord.x <- c(0.01, seq(0.01, teststat, 0.01), teststat)
cord.y <- c(0, dchisq(seq(0.01, teststat, 0.01), n - 1), 0)
polygon(cord.x,cord.y,col='darkorchid1')
polygon(cord.x,cord.y,col='darkorchid4', density = 3, lwd = 2)


cord.x <- c(teststat, seq(teststat, x.max, 0.01), x.max)
cord.y <- c(0, dchisq(seq(teststat, x.max, 0.01), n - 1), 0)
polygon(cord.x,cord.y,col='light blue')
polygon(cord.x,cord.y,col='navy', density = 7, angle = 135, lwd = 3)
}

# plot if high value
if (plow >= phigh) {
cord.x <- c(0.01, seq(0.01, teststat, 0.01), teststat)
cord.y <- c(0, dchisq(seq(0.01, teststat, 0.01), n - 1), 0)
polygon(cord.x,cord.y,col='light blue')
polygon(cord.x,cord.y,col='navy', density = 3, lwd = 2)

if (teststat < x.max) {
cord.x <- c(teststat, seq(teststat, x.max, 0.01), x.max)
cord.y <- c(0, dchisq(seq(teststat, x.max, 0.01), n - 1), 0)
polygon(cord.x,cord.y,col='darkorchid1')
polygon(cord.x,cord.y,col='darkorchid4', density = 7, angle = 135, lwd = 3)
}
}
}
   }


# end of gaussian model plots
} else {

      # plot asymptotic distribution
      if (input$method == 'gauss' | input$expmethod == 'expgauss') {

if (input$nbinom <= 10000 & input$npois <= 10000 & input$nexp <= 10000) {
        # get test statistic value
        teststat <- switch(input$dist,
               binom = abs(that - t0)/sqrt(t0*(1 - t0)/n),
               pois = abs(that - t0)/sqrt(t0/n),
               exp = abs(that - t0)/(t0/sqrt(n)))      
         pvalue <- 2*(1 - pnorm(teststat))  
         x.max <- 4
         x.min <- -x.max

        plot(0, 0, type = "n", xlim = c(x.min, x.max), ylim = c(0, 0.4), xlab = NA, ylab = NA, main = "G(0,1) Distribution", cex.axis = 2, yaxt = "n", cex.main = 2)
        curve(dnorm(x), col = 1, add = TRUE, lwd = 1.5)

text(-3, 0.35, bquote(theta[0] == .(t0)), cex = 1.5)
text(-3, 0.3,bquote(hat(theta) == .(that)), cex = 1.5)

text(2.5, 0.35, paste("Test statistic =", round(teststat, 3)), cex = 1.5)
if (pvalue >= 0.0001) {
  text(2.5, 0.3, paste("p-value =", round(pvalue, 4)), cex = 1.5)
} else {
  text(2.5, 0.3, paste("p-value < 0.0001"), cex = 1.5)
}

# plot interior
cord.x <- c(-teststat, seq(-teststat, teststat, 0.01), teststat)
cord.y <- c(0, dnorm(seq(-teststat, teststat, 0.01)), 0)
polygon(cord.x,cord.y,col='light blue')
polygon(cord.x,cord.y,col='navy', density = 3, lwd = 2)

# plot tails
if (teststat < 4) {
cord.x <- c(teststat, seq(teststat, x.max, 0.01), x.max)
cord.y <- c(0, dnorm(seq(teststat, x.max, 0.01)), 0)
polygon(cord.x,cord.y,col='lightpink')
polygon(cord.x,cord.y,col='indianred3', density = 7, angle = 135, lwd = 3)
cord.x <- c(x.min, seq(x.min, -teststat, 0.01), -teststat)
cord.y <- c(0, dnorm(seq(x.min, -teststat, 0.01)), 0)
polygon(cord.x,cord.y,col='lightpink')
polygon(cord.x,cord.y,col='indianred3', density = 7, angle = 135, lwd = 3)
}
}
# end of asymptotic gaussian plot
      }

      # plot LR test distribution
      if (input$method == 'lr'  | input$expmethod == 'explr') {
        # get test statistic value
#        if (that != t0) {
if (n <= 10000) {
        teststat <- switch(input$dist,
               binom = -2*((that*n)*log(t0/that)+(n-(that*n))*log((1-t0)/(1-that))),
               pois = -2*(n*that*log(t0/that)+n*(that-t0)),
               exp = -2*(n*log(that/t0)+n*(1-that/t0)))      
         pvalue <- 1 - pchisq(teststat, 1)  
}
#         }
#        if (that == t0) {
            
#        }
         x.max <- 10
         x.min <- 0

        plot(0, 0, type = "n", xlim = c(x.min, x.max), ylim = c(0, 1.5), xlab = NA, ylab = NA, main = expression(paste(chi^2, "(1) Distribution")), cex.axis = 2, yaxt = "n", cex.main = 2)
        curve(dchisq(x, 1), col = 1, add = TRUE, lwd = 1.5)

text(2.5, 0.6, bquote(theta[0] == .(t0)), cex = 1.5)
text(2.5, 0.4,bquote(hat(theta) == .(that)), cex = 1.5)

text(5, 0.6, paste("Test statistic =", round(teststat, 3)), cex = 1.5)
if (pvalue >= 0.0001) {
  text(5, 0.4, paste("p-value =", round(pvalue, 4)), cex = 1.5)
} else {
  text(5, 0.4, paste("p-value < 0.0001"), cex = 1.5)
}

# plot lower
cord.x <- c(0.01, seq(0.01, teststat, 0.01), teststat)
cord.y <- c(0, dchisq(seq(0.01, teststat, 0.01), 1), 0)
polygon(cord.x,cord.y,col='light blue')
polygon(cord.x,cord.y,col='navy', density = 3, lwd = 2)

# plot higher
if (teststat < 10) {
if (teststat != 0) {
cord.x <- c(teststat, seq(teststat, x.max, 0.01), x.max)
cord.y <- c(0, dchisq(seq(teststat, x.max, 0.01), 1), 0)
polygon(cord.x,cord.y,col='lightpink')
} else {
cord.x <- c(0.0001, seq(0.0001, x.max, 0.01), x.max)
cord.y <- c(0, dchisq(seq(0.0001, x.max, 0.0001), 1), 0)
}
polygon(cord.x,cord.y,col='indianred3', density = 7, angle = 135, lwd = 3)
}
# end of LR plot
}

      # plot exact test for exponential
      if (input$expmethod == 'expexact') {
        # get test statistic value
        teststat <- 2*n*that/t0
         plow <- 2*pchisq(teststat, 2*n)
         phigh <- 2*(1 - pchisq(teststat, 2*n))
         pvalue <- min(plow, phigh)
         x.max <- 4*n
         x.min <- 0

        plot(0, 0, type = "n", xlim = c(x.min, x.max), ylim = c(0, max(dchisq(n - 1, n - 1))), xlab = NA, ylab = NA, main = expression(paste(chi^2, " Distribution")), cex.axis = 2, yaxt = "n", cex.main = 2)
        curve(dchisq(x, 2*n), col = 1, add = TRUE, lwd = 1.5)

text(0.1*(2*n), 0.8*dchisq(2*n, 2*n), bquote(theta[0] == .(t0)), cex = 1.5)
text(0.1*(2*n), 0.6*dchisq(2*n, 2*n), bquote(hat(theta) == .(that)), cex = 1.5)

text(1.7*(2*n), 0.8*dchisq(2*n, 2*n), paste("Test statistic =", round(teststat, 3)), cex = 1.5)
if (pvalue >= 0.0001) {
  text(1.7*(2*n), 0.6*dchisq(2*n, 2*n), paste("p-value =", round(pvalue, 4)), cex = 1.5)
} else {
  text(1.7*(2*n), 0.6*dchisq(2*n, 2*n), paste("p-value < 0.0001"), cex = 1.5)
}


# plot if low value
if (plow < phigh) {
cord.x <- c(0.01, seq(0.01, teststat, 0.01), teststat)
cord.y <- c(0, dchisq(seq(0.01, teststat, 0.01), 2*n), 0)
polygon(cord.x,cord.y,col='darkorchid1')
polygon(cord.x,cord.y,col='darkorchid4', density = 3, lwd = 2)


cord.x <- c(teststat, seq(teststat, x.max, 0.01), x.max)
cord.y <- c(0, dchisq(seq(teststat, x.max, 0.01), 2*n), 0)
polygon(cord.x,cord.y,col='light blue')
polygon(cord.x,cord.y,col='navy', density = 7, angle = 135, lwd = 3)
}

# plot if high value
if (plow >= phigh) {
cord.x <- c(0.01, seq(0.01, teststat, 0.01), teststat)
cord.y <- c(0, dchisq(seq(0.01, teststat, 0.01), 2*n), 0)
polygon(cord.x,cord.y,col='light blue')
polygon(cord.x,cord.y,col='navy', density = 3, lwd = 2)

if (teststat < x.max) {
cord.x <- c(teststat, seq(teststat, x.max, 0.01), x.max)
cord.y <- c(0, dchisq(seq(teststat, x.max, 0.01), 2*n), 0)
polygon(cord.x,cord.y,col='darkorchid1')
polygon(cord.x,cord.y,col='darkorchid4', density = 7, angle = 135, lwd = 3)
}
}



# end of exact plot
}

# end of non-gaussian model plots
}
# end of renderplot
})
  # end of server
  }
# end of shinyApp
)



