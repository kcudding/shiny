### Data summaries, including ecdf/QQplot switching option

## TODO:
## change first tweet to

library(shiny)
library(rhandsontable)

shinyApp(
  ui <- fluidPage(
  # App title ----
  titlePanel("Probability experiments"),

  # Sidebar layout with input and output definitions ----
  sidebarLayout(

    # Sidebar panel for inputs ----
    sidebarPanel(
"Instruction text goes here.",

h2("", style = "font-size:25px;"),

textInput("password", "Enter password:", value = "", width = NULL, placeholder = NULL),

          conditionalPanel(
            condition = "input.password == 'dbod'",

      # Input: question success rate
      sliderInput("qsr", "Probability question correct:", value = 0.75, min = 0, max = 1, step = 0.05),

      # Input: slot given correct answer
      sliderInput("psc", "Probability slot given correct answer:", value = 0.7, min = 0, max = 1, step = 0.05),

      # Input: slot given incorrect answer
      sliderInput("psw", "Probability slot given incorrect answer:", value = 0.15, min = 0, max = 1, step = 0.05),

      # Input: prize values on tower
      sliderInput("n.prizes", "Non-zero prizes on tower:", value = 7, min = 0, max = 10, step = 1),

   # end of condition panel
      # Button
 #       fluidRow(
  #        column(12, align = 'center',
   #       actionButton("simulate", "Simulate"))
    #    )

   ),



#          conditionalPanel(
 #           condition = "input.password == 'dbod'",
#        fluidRow(
 #         column(12, align = 'center',
  #        actionButton("simulate", "Simulate"))
   #     )
  #        ),

    ),

    # Main panel for displaying outputs ----
    mainPanel(
#      tableOutput("orgtable")

        # output org tables
          conditionalPanel(
            condition = "input.password == 'dbod'",
        h2((textOutput("winText"))),
#          h2(("Tables give number of followers of each account at time of primary dataset download, and number of tweets (n) and date of first tweet (Start) per account in primary dataset."), style = "font-size:25px;"),

#         h2(strong("Personal"), style = "font-size:18px;"),
#         h2(strong("Organizations"), style = "font-size:18px;"),
#                           tableOutput("resTab")

    # end of condition panel
   ),
   # end of mainpanel
    )
  # end of sidebarlayout
  )
  # end of ui
  ),

  server <- function(input, output) {

  # generate sample of observations from selected distribution
  datasetInput <- reactive({
  # use action button to resample when clicked
#    input$simulate
# number of questions
qt <- 10

## final round question parameters
# question success rate
q.s.r <- input$qsr
# probability slot given correct answer
p.s.c <- input$psc
# probability slot given wrong answer
p.s.w <- input$psw
# number of 0 prizes on tower
n.spaces <- 10 - input$n.prizes

## prize list
prize.list <- c(rep(0, n.spaces), rep(1, qt - n.spaces))
# set to zero for number of spaces


## final round simulation

runs <- 10000
win.list <- rep(0, runs)
for (r in 1:runs) {
# generate question success
c <- rbinom(qt, 1, q.s.r)
# if at least one correct, set lose to 0, change to 1 if lose
if (sum(c) != 0) {lose <- 0}
# c is 0 if wrong, c is 1 if correct
# go through each question at a time and decide to slot or drop
slot <- rep(0, qt)

for (qn in 1:qt) {
  # if correct, c = 1 and slot with prob p.s.c
  # if incorrect, c = 0 and slot with prob p.s.w
  slot[qn] <- rbinom(1, 1, c[qn]*p.s.c + (1 - c[qn])*p.s.w)
  # flag as a loser if incorrect and slotted
  if (c[qn] == 0 & slot[qn] == 1) {lose <- 1}
}
# if a win, compute prize
if (lose == 0) {
  prize <- sum(prize.list*slot*c)
  win.list[r] <- prize
}

}

# theoretical win rate
# principle: generate total wrong and multiply by probability
# no wrong answers are slotted (1 - p.s.w)
#theory.win <- dbinom(0, 10, 1 - q.s.r) + dbinom(1, 10, 1 - q.s.r)*(1 - p.s.w) + dbinom(2, 10, 1 - q.s.r)*(1 - p.s.w)^2 + dbinom(3, 10, 1 - q.s.r)*(1 - p.s.w)^3 + dbinom(4, 10, 1 - q.s.r)*(1 - p.s.w)^4 + dbinom(5, 10, 1 - q.s.r)*(1 - p.s.w)^5 + dbinom(6, 10, 1 - q.s.r)*(1 - p.s.w)^6 + dbinom(7, 10, 1 - q.s.r)*(1 - p.s.w)^7 + dbinom(8, 10, 1 - q.s.r)*(1 - p.s.w)^8 + dbinom(9, 10, 1 - q.s.r)*(1 - p.s.w)^9

# note: if 0s on tower, we have to modify this? or just simulate?

win.rate <- sum(win.list > 0)/runs
win.rate
#cat(paste("Win rate:",sum(win.list > 0)/runs, "\n"))
#cat(paste("Theoretical", theory.win, "\n"))



  })


    output$winText <- renderText({
win.per <- datasetInput()*100
        paste("For this configuration, approximately ", win.per, "% of playes will win some money.", sep = "")
    })


  # Table of summary information ----
#  output$resTab <- renderTable({
#win.rate <- datasetInput()
#   data.frame(Username = c(1:3), win.rate = c(win.rate, win.rate, win.rate))
#  })


  #end or server
  }
# end of shinyApp
)



