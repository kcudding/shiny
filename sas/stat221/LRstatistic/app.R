
### Data summaries, including ecdf/QQplot switching option

library(shiny)
#library(rhandsontable)

shinyApp(
  ui <- fluidPage(
    titlePanel("Likelihood Ratio Statistic"),
    sidebarLayout(
      sidebarPanel("Explore the distribution of the likelihood ratio statistic. Generate multiple datasets and summarize the resulting value of the statistic. Note: maximum sample size and number of samples is n*N = 50000.",

        wellPanel(

#    dropdown list for distribution (use if distribution list gets too long)
#    selectInput("dist", "Select distribution:",
#              c("G(\u03BC,\u03C3)" = "norm",
#                         "Uniform(a, b)" = "unif",
#                         "Exponential(\u03B8)" = "exp",
#                         "Negative Exponential(\u03B8)" = "negexp",
#                         "t(k)" = "t")),

          # radio buttons for distribution
          radioButtons("dist", "Select distribution:",
#                       c("G(\u03BC,\u03C3)" = "norm",
                        c("Exponential(\u03B8)" = "exp",
#                         "Uniform(0, 2\u03B8)" = "unif",
#                         "t(k)" = "tdist", 
                         "Binomial(n, \u03B8)" = "binom",
                         "Poisson(\u03B8)" = "pois"), inline = F),

          # numeric input for sample size:
          numericInput(inputId = "n",
                       label = "Enter sample size (n)",
                       value = 100,
                       min = 10, max = 100000, step = 1),


          ## parameter values
          # normal
          conditionalPanel(
            condition = "input.dist == 'norm'",
            numericInput(inputId = "normMu",
                         label = "Enter \u03BC",
                         value = 0, min = -5, max = 5, step = 0.1),
            numericInput(inputId = "normSig",
                         label = "Enter \u03C3",
                         value = 1, min = 0, max = 5, step = 0.1),
          ),

          # exponential
          conditionalPanel(
            condition = "input.dist == 'exp'",
            numericInput(inputId = "expTheta",
                         label = "Enter \u03B8",
                         value = 1, min = 0.1, max = 5, step = 0.1),
          ),

          # Uniform: option if we want non-zero A
          conditionalPanel(
            condition = "1 == 2",
            numericInput(inputId = "unifa",
                         label = "Enter a",
                         value = 0, min = 0, max = 0, step = 0.1),
          ),
          conditionalPanel(
            condition = "input.dist == 'unif'",
#            numericInput(inputId = "unifa",
#                         label = "Enter a",
#                         value = 0, min = 0, max = 10, step = 0.1),
            numericInput(inputId = "unifb",
                         label = "Enter \u03B8",
                         value = 1, min = 0, max = 10, step = 0.1),
          ),

          # t
          conditionalPanel(
            condition = "input.dist == 'tdist'",
            numericInput(inputId = "tk",
                         label = "Enter k",
                         value = 3, min = 3, max = 100, step = 1),
          ),



          # Binomial
          conditionalPanel(
            condition = "input.dist == 'binom'",
            numericInput(inputId = "binomTheta",
                         label = "Enter \u03B8",
                         value = 0.5, min = 0.1, max = 0.9, step = 0.1),
          ),

          # Poisson
          conditionalPanel(
            condition = "input.dist == 'pois'",
            numericInput(inputId = "poisTheta",
                         label = "Enter \u03B8",
                         value = 5, min = 1, max = 30, step = 1),
          ),

          # numeric input for simulation runs:
          numericInput(inputId = "r",
                       label = "Enter number of samples (N)",
                       value = 100,
                       min = 10, max = 1000, step = 1),
          numericInput(inputId = "moe",
                       label = "Enter margin of error",
                       value = 0.1,
                       min = 0.1, max = 10, step = 0.1),

          # slider for number of bins in histogram
          sliderInput("bins", "Select number of bins:",
                      min = 1, max = 100, value = 10),

        # option to plot second sample size
            checkboxInput(inputId = "plotG", 
                          label = "Add chi-squared distribution curve?", value = FALSE, width = NULL),

#        checkboxInput(inputId = 'sample', label = "Show", value = FALSE, width = NULL),

          # resample button
          fluidRow(
            column(12, align = 'center',
            actionButton("resample", "Resample!"))
          )
        # end of wellPanel
        ),
      # end of sidebarPanel
      ),
      # create main panel for outputs

          conditionalPanel(
            condition = "input.n*input.r <= 50000",

     mainPanel(
        # output plots: distribution and sampling distribution
        fluidRow(
          splitLayout(cellWidths = c("50%", "50%"),
                      plotOutput("popPlot"),
                      plotOutput("sampdistPlot"))
        )
        # output table of first 10 estimates
#        textOutput("estsText1"),
#        textOutput("estsText2"),
#        tableOutput("estsTable"),
#        textOutput("estsText3"),

#tags$head(tags$style("#estsText1{font-size: 20px;
#                                 font-style: bold;
#                                 }"
#                         )
#              ),

#tags$head(tags$style("#estsText2{font-size: 20px;
#                                 font-style: bold;
#                                 }"
#                         )
#              ),

#tags$head(tags$style("#estsText3{font-size: 20px;
#                                 font-style: bold;
#                                 }"
#                         )
#              )

      # end of mainPanel
      )
# end of sample size condition
    )
    # end of sidebarLayout
    )
  # end of ui
  ),

  server <- function(input, output) {

    # generate sample of observations from selected distribution
    datasetInput <- reactive({
# condition
if (input$n*input$r <= 50000) {

    # use action button to resample when clicked
    input$resample
    switch(input$dist,
           norm = matrix(rnorm(input$n * input$r, input$normMu, input$normSig), ncol = input$r),
           unif =  matrix(runif(input$n * input$r, input$unifa, 2*input$unifb), ncol = input$r),
           tdist =  matrix(rt(input$n * input$r, input$tk), ncol = input$r),
           exp = matrix(rexp(input$n * input$r, 1/input$expTheta), ncol = input$r),
           binom = matrix(rbinom(input$n * input$r, 1, input$binomTheta), ncol = input$r),
           pois = matrix(rpois(input$n * input$r, input$poisTheta), ncol = input$r))
# end of sample size condition
}
    })

    # plot population distribution and first sample
    output$popPlot <- renderPlot({
      # retrieve repeated samples of observations
      sampFull <- datasetInput()
      # extract one sample
      s <- sampFull[,1]

      # normal distribution
if (input$dist == 'norm') {
      curve(dnorm(x, input$normMu, input$normSig),
            main = 'Population Distribution',
            xlab = '', ylab = NA,
            col = "red", lty = 1, lwd = 2, cex.axis = 2, cex.main = 2,
        ylim = c(0, dnorm(input$normMu, input$normMu, input$normSig)),
#            ylim = c(0, max(hist(s, breaks = input$bins)$density, dnorm(input$normMu, input$normMu, input$normSig))),
            xlim = c(input$normMu - 3*input$normSig, input$normMu + 3*input$normSig))

}

      # uniform distribution
if (input$dist == 'unif') {
      curve(dunif(x, input$unifa, 2*input$unifb),
            main = 'Population Distribution',
            xlab = 'Variate value', ylab = NA,
            col = "red", lty = 1, lwd = 2, cex.axis = 2, cex.main = 2,
        ylim = c(0, dunif(0.5*(input$unifa + input$unifb), input$unifa, input$unifb)),
#            ylim = c(0, max(hist(s, breaks = input$bins)$density, dunif(0.5*(input$unifa + 2*input$unifb), input$unifa, 2*input$unifb))),
            xlim = c(input$unifa - 0.1, 2*input$unifb + 0.1))

}

      # t distribution
if (input$dist == 'tdist') {
      sd <- 1
      if (input$tk > 2) {sd <- sqrt(input$tk/(input$tk - 2))}
      curve(dt(x, input$tk),
            main = 'Population Distribution',
            xlab = 'Variate value', ylab = NA,
            col = "red", lty = 1, lwd = 2, cex.axis = 2, cex.main = 2,
          ylim = c(0, dt(0, input$tk)),
#            ylim = c(0, max(hist(s, breaks = input$bins)$density, dt(0, input$tk))),
            xlim = c(-3*sd, 3*sd))

}

      # exponential distribution
if (input$dist == 'exp') {
      curve(dexp(x, 1/input$expTheta),
            main = 'Population Distribution',
            xlab = 'Variate value', ylab = NA,
            col = "red", lty = 1, lwd = 2, cex.axis = 2, cex.main = 2,
          ylim = c(0, dexp(0, 1/input$expTheta)),
#            ylim = c(0, max(hist(s, breaks = input$bins)$density, dexp(0, 1/input$expTheta))),
            xlim = c(max(0, input$expTheta - 3*input$expTheta^2), input$expTheta + 3*input$expTheta^2))

}

      # binomial distribution
if (input$dist == 'binom') {
      # set plot range
      binomLow <- max(0, round(input$n * input$binomTheta - 3.5*sqrt(input$n * input$binomTheta * (1 - input$binomTheta))))
      binomHigh <- min(round(input$n * input$binomTheta + 3.5*sqrt(input$n * input$binomTheta * (1 - input$binomTheta))), input$n)
      barplot(height = dbinom(c(binomLow:binomHigh), input$n, input$binomTheta),
            main = 'Population Distribution',
            xlab = 'Variate value', ylab = NA, cex.axis = 2, cex.main = 2,
            names.arg = c(binomLow:binomHigh),
            col = "forestgreen",
            angle = 45, density = 50,
            ylim = c(0, dbinom(round(input$n * input$binomTheta), input$n, input$binomTheta)))

}

      # Poisson distribution
if (input$dist == 'pois') {
      # set plot range
      poisLow <- max(0, round(input$poisTheta - 3*input$poisTheta))
      poisHigh <- round(input$poisTheta + 3*input$poisTheta)
      barplot(height = dpois(c(poisLow:poisHigh), input$poisTheta),
            main = 'Population Distribution',
            xlab = 'Variate value', ylab = NA, cex.axis = 2, cex.main = 2,
            names.arg = c(poisLow:poisHigh),
            col = "forestgreen",
            angle = 45, density = 50,
            ylim = c(0, dpois(input$poisTheta - 1, input$poisTheta)))

}

      # plot histogram of one sample if continuous
#if (input$dist %in% c('norm', 'exp')) {
#      hist(s, add = TRUE,
#           breaks = input$bins,
#           col = "dodgerblue3",
#           density = 27, angle = 43,
#           prob = T
#      )
#}

    })

    output$sampdistPlot <- renderPlot({
      # retrieve repeated samples of observations
      sampFull <- datasetInput()
      # sample size
      n <- dim(sampFull)[1]
      # LR functions
      # Poisson
      PoisRLF <- function(x, n, thetahat) {
		exp(n*thetahat*log(x/thetahat) + n*(thetahat - x))
	}
      # Binomial
      BinRLF <- function(x, y, n, thetahat) {
                    exp(y*log(x/thetahat)+(n-y)*log((1-x)/(1-thetahat)))
      }
      # exponential      
      ExpRLF<-function(x, n, thetahat) {
         exp(n*log(thetahat/x)+n*(1-thetahat/x))
      }

      # thetahat
      sampThetahat <- apply(sampFull, 2, mean)

      # LR stat
      sampEst <- switch(input$dist, 
                          binom = -2*log(BinRLF(input$binomTheta, sampThetahat*n, n, sampThetahat)), 
                          pois = -2*log(PoisRLF(input$poisTheta, n, sampThetahat)),
                          exp = -2*log(ExpRLF(input$expTheta, n, sampThetahat)))

#      sampEst <- apply(sampFull, 2, mean)

# what are we estimating
sampStat <- switch(input$dist,
                   binom = 'Proportion',
                   'Mean')

      # and true chi-squared distribution curve
      curve(dchisq(x, 1),
            main = 'Sampling Distribution of LR Statistic',
            xlab = 'Sample LR statistic', ylab = NA, cex.axis = 2, cex.lab = 2, cex.main = 2,
            col = "red", lty = 1, lwd = 2*input$plotG,
            ylim = c(0, max(hist(sampEst, breaks = input$bins)$density, 1.5)),
            xlim = c(0, max(sampEst, 5)))

      # plot histogram of sample estimates
      hist(sampEst, add = TRUE,
           breaks = input$bins,
           col = "dodgerblue3",
           density = 27, angle = 43,
           prob = T
      )


    })

    output$estsText1 <- renderText({
        paste("You generated N = ", input$r, " datasets, each of size n = ", input$n, ".", sep = "")
    })

        

    output$estsText2 <- renderText({
        # parameter notation for output (mu or theta)
        param <- switch(input$dist,
                        norm = '\u03BC',
                        tdist = 'the population mean',
                        '\u03B8')
        paste("The sample estimates of", param, "resulting from the first 10 samples were:")
    })

    output$estsTable <- renderTable(digits = 3, {
      sampFull <- datasetInput()
      ests <- data.frame(t(apply(sampFull[,1:10], 2, mean)))
      # add column names
      colnames(ests) <- paste("Sample", 1:10)
      ests
    })

    output$estsText3 <- renderText({
        # parameter notation for output (mu or theta)
        param <- switch(input$dist,
                        norm = '\u03BC',
                        tdist = 'the population mean',
                        '\u03B8')
        # true parameter value for output
        trueParam <- switch(input$dist,
               norm = input$normMu,
               exp = input$expTheta,
               unif = input$unifb,
               tdist = 0,
               binom = input$binomTheta,
               pois = input$poisTheta)
        # percentage of estimates within 1 unit of truth
        sampFull <- datasetInput()
        closeCount <- sum(abs(apply(sampFull, 2, mean) - trueParam) < input$moe)

        paste("Of the ", input$r, " sample estimates of ", param, " that were generated, ", closeCount, " (", round(100*closeCount/input$r, 1), "%) were within ", input$moe, " of the true value ", param, " = ", trueParam, sep = "")
    })

  #end or server
  }
# end of shinyApp
)



