
### Likelihood intervals and principles of repeated sampling

library(shiny)
library(rhandsontable)

shinyApp(
  ui <- fluidPage(
    titlePanel("Likelihood Functions"),
    sidebarLayout(
      sidebarPanel("Explore the relative likelihood and log relative likelihood functions for various probability distributions. Select the function you wish to plot for your chosen distribution. Then enter a sample size and a value for the maximum likelihood estimate of \u03B8 to see what the corresponding function looks like. You can also add a second line and a second sample size.",
h2(""),
        wellPanel(
          # radio buttons for plot type
          radioButtons("func", "Select function:",
                       c("Relative likelihood" = "rel",
                         "Log relative likelihood" = "logrel"), inline = F),

          # radio buttons for distribution
          radioButtons("dist", "Select distribution:",
                       c("Binomial (n, \u03B8)" = "binom",
                         "Poisson(\u03B8)" = "pois",
                         "Exponential (\u03B8)" = "exp"), inline = F),
	  # numeric or slider input
          radioButtons("numsli", "Numeric or slider input?",
                       c("Numeric" = "num",
                         "Slider" = "sli"), inline = F),


          # numeric input for sample size:
          conditionalPanel(
            condition = "input.numsli == 'num'",

          numericInput(inputId = "n",
                       label = "Enter sample size",
                       value = 50, min = 10, max = 100000, step = 1),


          # numeric inputs for sample information:
          conditionalPanel(
            condition = "input.dist == 'binom'",
            numericInput(inputId = "biny",
                         label = "Enter sample proportion",
                         value = 0.5, min = 0, max = 1, step = 0.1),
          ),

          conditionalPanel(
            condition = "input.dist == 'pois'",
            numericInput(inputId = "poisybar",
                         label = "Enter sample mean",
                         value = 5, min = 1, max = NA, step = 1),
          ),

          conditionalPanel(
            condition = "input.dist == 'exp'",
            numericInput(inputId = "expybar",
                         label = "Enter sample mean",
                         value = 50, min = 0, max = NA, step = 1),
          ),
),

          # slider input for sample size:
          conditionalPanel(
            condition = "input.numsli == 'sli'",

          sliderInput(inputId = "n.sli",
                       label = "Select sample size",
                       value = 50, min = 10, max = 1000, step = 10),


          # slider inputs for sample information:
          conditionalPanel(
            condition = "input.dist == 'binom'",
            sliderInput(inputId = "biny.sli",
                         label = "Select sample proportion",
                         value = 0.5, min = 0, max = 1, step = 0.05),
          ),

          conditionalPanel(
            condition = "input.dist == 'pois'",
            sliderInput(inputId = "poisybar.sli",
                         label = "Select sample mean",
                         value = 5, min = 1, max = 20, step = 1),
          ),

          conditionalPanel(
            condition = "input.dist == 'exp'",
            sliderInput(inputId = "expybar.sli",
                         label = "Select sample mean",
                         value = 5, min = 1, max = 20, step = 1),
          ),
),

        # option to plot second sample size
            checkboxInput(inputId = "plotn2", 
                          label = "Add second sample size", value = FALSE, width = NULL),

          conditionalPanel(
            condition = "input.plotn2 == 1",

          conditionalPanel(
            condition = "input.numsli == 'num'",

          numericInput(inputId = "n2",
                       label = "Enter second sample size",
                       value = 100, min = 10, max = 100000, step = 1),
         ),
          conditionalPanel(
            condition = "input.numsli == 'sli'",

          sliderInput(inputId = "n2.sli",
                       label = "Select second sample size",
                       value = 100, min = 10, max = 1000, step = 1),
         ),

         ), 


       # end of wellPanel 
      ),
      # end of sidebarPanel
    ),

      # create main panel for outputs
      mainPanel(
        # output plots
        plotOutput("likePlot"),
      # end of mainPanel
      )
    # end of sidebarLayout
    )
  # end of ui
  ),

  server <- function(input, output) {

    output$likePlot <- renderPlot({
layout(c(1,2), heights=c(6, 1))
      # plot relative likelihood function for binomial
      if (input$dist == 'binom') {
	# likelihood function
	BinLF <- function(x, y, n) {
		exp(log(choose(n, y)) + y*log(x) + (n-y)*log(1-x))
        }
	# log likelihood function
	BinlogLF <- function(x, y, n) {
		log(choose(n, y)) + y*log(x) + (n-y)*log(1-x)
        }
        # relative likelihood function
        BinRLF <- function(x, y, n, thetahat) {
                    exp(y*log(x/thetahat)+(n-y)*log((1-x)/(1-thetahat)))}
	# log relative likelihood function
	BinlogRLF <- function(x, y, n, thetahat) {
                    y*log(x/thetahat)+(n-y)*log((1-x)/(1-thetahat))}


        # retrieve sample of observations
	if (input$numsli == 'num') {
        n <- input$n
        y <- input$biny*input$n
        thetahat <- input$biny
        s <-(thetahat*(1-thetahat)/n)^0.5
	th <- seq(max(0, thetahat - 4*s), min(1, thetahat + 4*s), 0.001)
          if (input$plotn2 == 1) {
            n2 <- input$n2
            y2 <- input$biny*input$n2
	  }
	}
	if (input$numsli == 'sli') {
        n <- input$n.sli
        y <- input$biny.sli*input$n.sli
        thetahat <- input$biny.sli
        s <-(thetahat*(1-thetahat)/n)^0.5
	th <- seq(0, 1, 0.001)
          if (input$plotn2 == 1) {
            n2 <- input$n2.sli
            y2 <- input$biny.sli*input$n2.sli
	  }
	}


if (input$func == "like") {

        plot(th, BinLF(th, y, n) , xlab = expression(theta), ylab = expression(paste("L(",theta,")")), type = "l", lwd = 2, main = "Binomial likelihood function")

       # add second sample size if specified
       if (input$plotn2 == 1) {
        points(th, BinLF(th, y2, n2), col = 2, type = "l", lwd = 2, lty = 2)
       }
}

if (input$func == "loglike") {

        plot(th, BinlogLF(th, y, n) , xlab = expression(theta), ylab = expression(paste("l(",theta,")")), type = "l", lwd = 2, main = "Binomial log likelihood function")

       # add second sample size if specified
       if (input$plotn2 == 1) {
        points(th, BinlogLF(th, y2, n2), col = 2, type = "l", lwd = 2, lty = 2)
       }
}

if (input$func == "rel") {

        plot(th, BinRLF(th, y, n, thetahat) , xlab = expression(theta), ylab = expression(paste("R(",theta,")")), type = "l", lwd = 2, main = "Binomial relative likelihood function")

       # add second sample size if specified
       if (input$plotn2 == 1) {
        points(th, BinRLF(th, y2, n2, thetahat), col = 2, type = "l", lwd = 2, lty = 2)
       }
}

if (input$func == "logrel") {
        plot(th, BinlogRLF(th, y, n, thetahat) , xlab = expression(theta), ylab = expression(paste("r(",theta,")")), type = "l", lwd = 2, , main = "Binomial log relative likelihood function")

       # add second sample size if specified
       if (input$plotn2 == 1) {
        points(th, BinlogRLF(th, y2, n2, thetahat), col = 2, type = "l", lwd = 2, lty = 2)
       }
}

      }

     # plot for Poisson
      if (input$dist == 'pois') {
	# likelihood function
	PoisLF <- function(x, n, ybar) {
		exp(ybar*n*log(x) - n*x)
        }
	# log likelihood function
	PoislogLF <- function(x, n, ybar) {
		ybar*n*log(x) - n*x
        }
        # relative likelihood function
        PoisRLF <- function(x, n, thetahat) {
		exp(n*thetahat*log(x/thetahat) + n*(thetahat - x))
	}
	# log relative likelihood function
	PoislogRLF <- function(x, n, thetahat) {
		n*thetahat*log(x/thetahat) + n*(thetahat - x)
	}


        # retrieve sample of observations
	if (input$numsli == 'num') {
        n <- input$n
        ybar <- input$poisybar
        thetahat <- input$poisybar
	s<-(thetahat/n)^0.5
	th <- seq(max(0, thetahat - 4*s), thetahat + 4*s,0.001)
	  if (input$plotn2 == 1) {
           n2 <- input$n2
           y2 <- input$biny*input$n2
	  }
	}
	if (input$numsli == 'sli') {
        n <- input$n.sli
        ybar <- input$poisybar.sli
        thetahat <- input$poisybar.sli
	s <- (thetahat/n)^0.5
	th <- seq(max(0, thetahat - 4*s), thetahat + 4*s,0.001)
	  if (input$plotn2 == 1) {
           n2 <- input$n2.sli
           y2 <- input$biny*input$n2.sli
	  }
	}

if (input$func == "like") {

        plot(th, PoisLF(th, n, ybar) , xlab = expression(theta), ylab = expression(paste("L(",theta,")")), type = "l", lwd = 2, main = "Poisson likelihood function")

       # add second sample size if specified
       if (input$plotn2 == 1) {
        points(th, PoisLF(th, n2, y2), col = 2, type = "l", lwd = 2, lty = 2)
       }
}

if (input$func == "loglike") {

        plot(th, PoislogLF(th, n, thetahat) , xlab = expression(theta), ylab = expression(paste("l(",theta,")")), type = "l", lwd = 2, main = "Poisson log likelihood function")

       # add second sample size if specified
       if (input$plotn2 == 1) {
        points(th, PoislogLF(th, n2, thetahat), col = 2, type = "l", lwd = 2, lty = 2)
       }
}

if (input$func == "rel") {

        plot(th, PoisRLF(th, n, thetahat) , xlab = expression(theta), ylab = expression(paste("R(",theta,")")), type = "l", lwd = 2, main = "Poisson relative likelihood function")

       # add second sample size if specified
       if (input$plotn2 == 1) {
        points(th, PoisRLF(th, n2, thetahat), col = 2, type = "l", lwd = 2, lty = 2)
       }
}

if (input$func == "logrel") {
        plot(th, PoislogRLF(th, n, thetahat) , xlab = expression(theta), ylab = expression(paste("r(",theta,")")), type = "l", lwd = 2, , main = "Poisson log relative likelihood function")

       # add second sample size if specified
       if (input$plotn2 == 1) {
        points(th, PoislogRLF(th, n2, thetahat), col = 2, type = "l", lwd = 2, lty = 2)
       }
}


      }


	# exponential distribution
      if (input$dist == 'exp') {
	# likelihood function
	ExpLF <- function(x, n, ybar) {exp(-n*log(x) - n*ybar/x)}
	# log likelihood function
	ExplogLF <- function(x, n, ybar) {-n*log(x) - n*ybar/x}
        # relative likelihood function
        ExpRLF <- function(x, n, thetahat) {exp(n*log(thetahat/x) + n*(1-thetahat/x))}
	# log relative likelihood function
        ExplogRLF <- function(x, n, thetahat) {n*log(thetahat/x) + n*(1-thetahat/x)}

	if (input$numsli == 'num') {
        theta <- input$exptheta
        n <- input$n
        thetahat <- ybar <- input$expybar
        s <- thetahat/(n^0.5)
        th<-seq(max(0,thetahat-3*s), thetahat+4*s,0.001)
       if (input$plotn2 == 1) {
          n2 <- input$n2
       }
	}
	if (input$numsli == 'sli') {
        theta <- input$exptheta.sli
        n <- input$n.sli
        thetahat <- ybar <- input$expybar.sli
        s <- thetahat/(n^0.5)
        th<-seq(max(0,thetahat-3*s), thetahat+4*s,0.001)
       if (input$plotn2 == 1) {
          n2 <- input$n2.sli
       }
	}

if (input$func == "like") {
        plot(th, ExpRLF(th, n, ybar) , xlab = expression(theta), ylab = expression(paste("L(",theta,")")), type = "l", lwd = 2, , main = "Exponential likelihood function")

       # add second sample size if specified
       if (input$plotn2 == 1) {
        points(th, ExpRLF(th, n2, ybar), col = 2, type = "l", lwd = 2, lty = 2)
       }
}

if (input$func == "loglike") {
        plot(th, ExplogLF(th, n, ybar) , xlab = expression(theta), ylab = expression(paste("l(",theta,")")), type = "l", lwd = 2, , main = "Exponential log likelihood function")

       # add second sample size if specified
       if (input$plotn2 == 1) {
        points(th, ExplogLF(th, n2, ybar), col = 2, type = "l", lwd = 2, lty = 2)
       }
}

if (input$func == "rel") {
        plot(th, ExpRLF(th, n, thetahat) , xlab = expression(theta), ylab = expression(paste("R(",theta,")")), type = "l", lwd = 2, , main = "Exponential relative likelihood function")

       # add second sample size if specified
       if (input$plotn2 == 1) {
        points(th, ExpRLF(th, n2, thetahat), col = 2, type = "l", lwd = 2, lty = 2)
       }
}

if (input$func == "logrel") {
        plot(th, ExplogRLF(th, n, thetahat) , xlab = expression(theta), ylab = expression(paste("r(",theta,")")), type = "l", lwd = 2, main = "Exponential log relative likelihood function")

       # add second sample size if specified
       if (input$plotn2 == 1) {
        points(th, ExplogRLF(th, n2, thetahat), col = 2, type = "l", lwd = 2, lty = 2)
       }
}

# end of exponential
      }

if (input$plotn2 == 1) {
par(mai=c(0,0,0,0))
plot.new()
legend(x="center", legend = c(paste("n =", n), paste("n =", n2)), col = c(1, 2), lty = c(1, 2), horiz = T)
}

# end of outputplot
    })
  # end of server
  }
# end of shinyApp
)



