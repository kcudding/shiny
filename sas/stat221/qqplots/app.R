
# STAT 231 Assignment 1: Do it for the (insta)gram

library(shiny)
library(rhandsontable)

shinyApp(

  ui <- fluidPage(
    titlePanel("STAT 231: QQ Plots"),
    sidebarLayout(
      sidebarPanel("Randomly generate datasets: new samples can be generated automatically by changing distribution, sample size, or clicking the 'Resample!' button.",

        wellPanel(
          # radio buttons for distribution
          radioButtons("dist", "Select distribution:",
                       c("Normal" = "rnorm",
                         "Uniform" = "runif",
                         "Exponential" = "rexp",
                         "Negative log-normal" = "lnorm",
                         "t" = "t"), inline = F),

          # numeric input for sample size:
          numericInput(inputId = "n",
                       label = "Enter sample size",
                       value = 100,
                       min = 10, max = 100000, step = 1),

          # slider for number of bins in histogram
          sliderInput("bins", "Select number of bins:",
                      min = 1, max = 100, value = 10),

        fluidRow(
          column(12, align = 'center',
          actionButton("resample", "Resample!"))
        )
      # end of wellPanel
      ),
    # end of sidebarPanel
    ),
  # create main panel for outputs
  mainPanel(
          # error message if n*r > 100000
                conditionalPanel(
                  condition = "input.n > 10000",
img(src='pikachu.png', align = "center", width = "500px"),
                  textOutput("nerrorText"),
                  tags$head(tags$style("#nerrorText{font-size: 20px;
                                                 color: red;
                                                 font-style: bold;
                                                }"
                                      )
                           )          
                ),


    # output plots
    fluidRow(
      splitLayout(cellWidths = c("50%", "50%"),
                  conditionalPanel(condition = "input.n <= 10000",plotOutput("histPlot")),
                  conditionalPanel(condition = "input.n <= 10000",plotOutput("qqPlot")))
    ),
    # output summary statistics
    conditionalPanel(condition = "input.n <= 10000",tableOutput("dataSummary"))

  # end of mainPanel
  )
# end of sidebarLayout
)
# end of ui
),

server <- function(input, output) {

  # generate sample of observations from selected distribution
  datasetInput <- reactive({
  # use action button to resample when clicked
  input$resample
if (input$n <= 10000) {
  switch(input$dist,
         lnorm = -rlnorm(input$n),
         t = rt(input$n, 3),
         get(input$dist)(input$n))
}
  })

  output$histPlot <- renderPlot({
    # retrieve sample of observations
    s <- datasetInput()
    # plot histogram
    hist(s,
         breaks = input$bins,
         col = "dodgerblue3",
         density = 27, angle = 43,
         xlab = "Sample value",
         main = "Histogram of Sample",
         prob = T,
         ylim = c(0, max(hist(s, breaks = input$bins)$density, dnorm(mean(s), mean(s), sd(s)))))
         # and normal ditsribution curve based on x
         curve(dnorm(x, mean(s),sd(s)),
               col = "red", add = TRUE, lty = 1, lwd = 2)
  })

  ## qq plot
  output$qqPlot <- renderPlot({
    # retrieve sample of observations
    s <- datasetInput()
    # qq plot
    qqnorm(s,
           xlab = 'G(0,1) Theoretical Quantiles',
           main = "QQ plot of Sample",
           col = 'navy')
           qqline(s, col = "red", lwd = 2)
    })

    # skewness function
    skewness <- function(x) {
      n <- length(x)
      xbar <- mean(x)
      numerator <- (1/n)*sum((x - xbar)^3)
      denominator <- ((1/n)*sum((x - xbar)^2))^(3/2)
      numerator/denominator
    }

    # kurtosis function
    kurtosis <- function(x) {
      n <- length(x)
      xbar <- mean(x)
      numerator <- (1/n)*sum((x - xbar)^4)
      denominator <- ((1/n)*sum((x - xbar)^2))^(2)
      numerator/denominator
    }


    output$dataSummary <- renderTable(digits = 3,{
      # retrieve sample of observations
      s <- datasetInput()
      # return summary statistics
      data.frame(Statistic = c('Mean', 'Median'), Value = c(mean(s), median(s)), Statistic = c('Variance', 'IQR'), Value = c(var(s), quantile(s, 0.75) - quantile(s, 0.25)), Statistic = c('Skewness', 'Kurtosis'), Value = c(skewness(s), kurtosis(s)), check.names = F)
    })
  #end or server
  }
# end of shinyApp
)



