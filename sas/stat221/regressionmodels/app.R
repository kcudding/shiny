
# STAT 231 Assignment 1: Do it for the (insta)gram

library(shiny)
library(rhandsontable)

shinyApp(

ui <- fluidPage(
  titlePanel("Scatterplots"),
  sidebarLayout(
    sidebarPanel(

      wellPanel(
        # radio buttons for distribution
        radioButtons("truemodel", "Choose true relationship:",
                     c("Random" = "rand",
                       "Linear" = "lin",
                       "Quadratic" = "quad"), inline = F),
         # checkbox to transform x if not qudratic
#        conditionalPanel(
#          condition = "input.truemodel != 'quad'",checkboxInput(inputId = "xtrans", label = "Transform x to sqrt(x)?", value = FALSE, width = NULL)),

        # numeric input for sample size:
        numericInput(inputId = "n",
                     label = "Enter sample size",
                     value = 100, min = 10, max = 100000, step = 1),

        # numeric inputs for linear model parameters:
        conditionalPanel(
          condition = "input.truemodel == 'lin'",
          numericInput(inputId = "linAlpha",
                       label = "Enter the y intercept",
                       value = 1, min = -100, max = 100, step = 1),
          numericInput(inputId = "linBeta",
                       label = "Enter the coefficient of x (the slope)",
                       value = 1, min = -100, max = 100, step = 1),
        ),

        # numeric inputs for quadratic model parameters:
        conditionalPanel(
          condition = "input.truemodel == 'quad'",
          numericInput(inputId = "quadAlpha",label = "Enter the y intercept", value = 1, min = -100, max = 100, step = 1),
          numericInput(inputId = "quadBeta",label = "Enter the cofficient of x", value = 1, min = -100, max = 100, step = 1),
          numericInput(inputId = "quadGamma",label = "Enter the coefficient of x \u00B2", value = 1, min = -100, max = 100, step = 1),
        ),

        # slider for size of variance
        conditionalPanel(
          condition = "input.truemodel != 'rand'",
          sliderInput("var", "Select standard deviation:",
                      min = 0, max = 1,
                      value = 0.5),
        radioButtons("heterosced", "Choose variability behaviour:",
                     c("Homoscedastic" = "homo",
                       "Heteroscedastic" = "hetero"), inline = T),
        ),

        # specify model to fit
        radioButtons("fitmodel", "Select model to fit:",
                     c("Linear: y = \u03B1 + \u03B2 x" = "linmod",
                       "Quadratic: y = \u03B1 + \u03B2 x + \u03B3 x \u00B2" = "quadmod"), inline = F),

        # specify second plot
        radioButtons("resplot", "Choose residual plot:",
                     c("Standardized Residuals vs x" = "resx",
                       "Standardized Residuals vs fitted" = "resfit",
                       "QQ plot of standardized residuals" = "resqq"), inline = F),

        fluidRow(
          column(12, align = 'center',
            actionButton("resample", "Resample!"))
        )
      # end of wellpanel
      ),
    # end of sidebarpanel
    ),

    # create main panel for outputs
    mainPanel(
# error if sample size too large
                conditionalPanel(
                  condition = "input.n > 1000",
img(src='pikachu.png', align = "center", width = "500px"),
                  textOutput("nerrorText"),
                  tags$head(tags$style("#nerrorText{font-size: 20px;
                                                 color: red;
                                                 font-style: bold;
                                                }"
                                      )
                           )          
                ),

      # output plots
                conditionalPanel(
                  condition = "input.n <= 1000",
        fluidRow(
          splitLayout(cellWidths = c("50%", "50%"),
                      plotOutput("scatterPlot"),
                      plotOutput("secondPlot"))
        ),

                  # output table of first 10 estimates
#                  textOutput("modelText"),
#                  tags$head(tags$style("#modelText{font-size: 20px;
#                                                 font-style: bold;
#                                                }"
#                                      )
#                           )
),

# output regression model details
conditionalPanel(condition = "input.n <= 1000", h4("Coefficients:")),

conditionalPanel(condition = "input.fitmodel == 'linmod' & input.n <= 1000", tableOutput("linregOut")),                  
conditionalPanel(condition = "input.fitmodel == 'quadmod' & input.n <= 1000", tableOutput("quadregOut"))
    )




  # end of sidebarlayout
  )
# end of ui
),

server <- function(input, output) {

  # generate sample of observations from selected distribution
  datasetInput <- reactive({
  input$resample
  # generate x values
if (input$n <= 1000) {
  x <- switch(input$truemodel, 
              rand = runif(input$n, 0, 5),
              lin = runif(input$n, 0, 5),
              quad = runif(input$n, -2.5, 2.5))
  # transform if require
#  if (input$xtrans == 1) {
#      x <- sqrt(x)
#   }
  # function to control heteroscedasticity
  heterovar <- x*as.numeric(input$heterosced == 'hetero') + 1*as.numeric(input$heterosced == 'homo')

  y <- switch(input$truemodel,
              rand = runif(input$n, -2.5, 2.5),
              lin = input$linAlpha + input$linBeta * x + heterovar * rnorm(input$n, 0, 3*input$var),
              quad = input$quadAlpha + input$quadBeta * x + heterovar * input$quadGamma * x^2 + rnorm(input$n, 0, 3*input$var))
  # return x, y
  cbind(x, y)
}
  })

  # generate scatter plot
  output$scatterPlot <- renderPlot({

    # retrieve sample of observations
    Data <- datasetInput()
    x <- Data[, 1]
    y <- Data[, 2]
    # plot data
      plot(x, y, pch = 21, col = 'navy', bg = 'dodgerblue3', cex = 2, cex.axis = 2, cex.lab = 2, lwd = 2, main = "Scatter Plot With Fitted Model")

      if (input$fitmodel == 'linmod') {
        abline(coef(lm(y ~ x)), lty = 2, lwd = 4, col = 2)
      }

      if (input$fitmodel == 'quadmod') {
        # fit quadratic model
        quad.mod <- lm(y ~ x + I(x^2))
        # define quadratic function to add to plot
        quad.fun <- function(x) {coef(quad.mod)[1] + coef(quad.mod)[2]*x + coef(quad.mod)[3]*x*x}
        # add to plot
        plot (quad.fun, min(x), max(x), add = TRUE, lwd = 4, lty = 2, col = 2)
      }

    })

    ## second plot: either ecdf or QQ dependent on input
    output$secondPlot <- renderPlot({
      Data <- datasetInput()
      x <- Data[, 1]
      y <- Data[, 2]

      if (input$fitmodel == 'linmod') {
        RegModel<-lm(y ~ x)
      }
      if (input$fitmodel == 'quadmod') {
        RegModel<-lm(y ~ x + I(x^2))
      }

      se <-summary(RegModel)$sigma        # estimate of sigma
      muhat <-RegModel$fitted.values      # fitted responses
      r <- RegModel$residuals             # residuals
      rstar <- r/se                      # standardized residuals
par(mar = c(5.1, 5.1, 2.1, 1.1))
      # default is to show residuals vs. x
if (input$resplot == 'resx') {
      plot(x, rstar, pch = 21, col = 'chartreuse4', bg = 'chartreuse2', cex = 2, cex.axis = 2, cex.lab = 2, lwd = 2, main = "Standardized Residuals vs x", xlab = "x", ylab = "Standardized Residual")
        abline(0, 0, col = "navy", lwd = 4, lty = 3)
}
      # residuals vs muhat
if (input$resplot == 'resfit') {
        plot(muhat, rstar, pch = 21, col = 'chartreuse4', bg = 'chartreuse2', cex = 2, cex.axis = 2, cex.lab = 2, lwd = 2, main = "Standardized Residuals vs Fitted", xlab = "Fitted value", ylab = "Standardized Residual")
        abline(0, 0, col = "navy", lwd = 4, lty = 3)
}
      # qqplot of residuals
if (input$resplot == 'resqq') {
        qqnorm(rstar, pch = 21, col = 'chartreuse4', bg = 'chartreuse2', cex = 2, cex.axis = 2, cex.lab = 2, lwd = 2, main = "QQ Plot of Standardized Residuals")
        qqline(rstar, col = "navy", lwd = 4, lty = 3)  # add line for comparison
}
    })

# model fit output
output$modelText <- renderText({

      Data <- datasetInput()
      x <- Data[, 1]
      y <- Data[, 2]

signchoice <- c("-", "+")
      if (input$fitmodel == 'linmod') {
        lsest <- lm(y ~ x)$coef
paste("Estimated Model: y = ", round(lsest[1], 3), " ", signchoice[1 + (sign(lsest[2]) + 1)/2], " ", abs(round(lsest[2], 3)), "x", sep = "")
      } else {
        if (input$fitmodel == 'quadmod') {
          lsest <- lm(y ~ x + I(x^2))$coef
paste("Estimated Model: y = ", round(lsest[1], 3), " ", signchoice[1 + (sign(lsest[2]) + 1)/2], " ", abs(round(lsest[2], 3)), "x", " ", signchoice[1 + (sign(lsest[3]) + 1)/2], " ", abs(round(lsest[3], 3)), "x\u00B2", sep = "")
        }
      }


})

    output$nerrorText <- renderText({
         "Oh no! Your n is too big!"
    })

    output$linregOut <- renderTable(digits = 5, rownames = T, {
      # retrieve sample of observations
      Data <- datasetInput()
      x <- Data[, 1]
      y <- Data[, 2]
      # fit model
      mod <- summary(lm(y~x))

      # return results
      mod$coefficients
    })

    output$quadregOut <- renderTable(digits = 5, rownames = T, {
      # retrieve sample of observations
      Data <- datasetInput()
      x <- Data[, 1]
      y <- Data[, 2]
      # fit model
      mod <- summary(lm(y~x + I(x^2)))

      # return results
      mod$coefficients
    })

# end of server
}
# end of shinyApp
)



