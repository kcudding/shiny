# STAT 231 Assignment 1: Do it for the (insta)gram

library(shiny)
library(rhandsontable)

shinyApp(

ui <- fluidPage(
  titlePanel("Scatterplots"),
  sidebarLayout(
    sidebarPanel(

      wellPanel(
        # radio buttons for distribution
        radioButtons("model", "Select relationship:",
                     c("Random" = "rand",
                       "Linear" = "lin",
                       "Quadratic" = "quad"), inline = F),

        # numeric input for sample size:
        numericInput(inputId = "n",label = "Enter sample size", value = 100, min = 10, max = 100000, step = 1),

        # numeric inputs for linear model parameters:
        conditionalPanel(
          condition = "input.model == 'lin'",
          numericInput(inputId = "linAlpha",label = "Enter \u03B1", value = 1, min = -100, max = 100, step = 1),
          numericInput(inputId = "linBeta",label = "Enter \u03B2", value = 1, min = -100, max = 100, step = 1),
        ),

        # numeric inputs for quadratic model parameters:
        conditionalPanel(
          condition = "input.model == 'quad'",
          numericInput(inputId = "quadAlpha",label = "Enter \u03B1", value = 1, min = -100, max = 100, step = 1),
          numericInput(inputId = "quadBeta",label = "Enter \u03B2", value = 1, min = -100, max = 100, step = 1),
        ),

        # slider for number of bins in histogram
        conditionalPanel(
          condition = "input.model != 'rand'",
          sliderInput("var", "Select variance:",
                      min = 0, max = 1,
                      value = 0.5),
        ),

        checkboxInput(inputId = 'linmod', label = "Fit linear model", value = FALSE, width = NULL),

        checkboxInput(inputId = 'quadmod', label = "Fit quadratic model", value = FALSE, width = NULL),

      ),



    fluidRow(
      column(12, align = 'center',
        actionButton("resample", "Resample!"))
    )


    ),
   # create main panel for outputs
    mainPanel(
   # output plots
plotOutput("scatterPlot")

    )
  )
),

server <- function(input, output) {

# generate sample of observations from selected distribution
datasetInput <- reactive({
        if (input$n > 1000000) {} else {
input$resample
# generate x values
x <- runif(input$n, -2.5, 2.5)


y <- switch(input$model,
            rand = runif(input$n, -2.5, 2.5),
            lin = input$linAlpha + input$linBeta * x + rnorm(input$n, 0, 3*input$var),
            quad = input$quadAlpha + input$quadBeta * x^2 + rnorm(input$n, 0, 3*input$var))

cbind(x, y)
        }
  })

  output$scatterPlot <- renderPlot({

# retrieve sample of observations
Data <- datasetInput()
x <- Data[, 1]
y <- Data[, 2]
# plot histogram
	plot(x, y, pch = 21, col = 'navy', bg = 'dodgerblue3', cex = 2, cex.axis = 2, cex.lab = 2, lwd = 2, main = paste('Sample correlation =', round(cor(x, y), 3)))

	if (input$linmod == T) {
		abline(coef(lm(y~x)), lty = 2, lwd = 3, col = 2)
	}

	if (input$quadmod == T) {
	  # fit quadratic model
	  quad.mod <- lm(y ~ x + I(x^2))
	  # define quadratic function to add to plot
quad.fun <- function(x) {coef(quad.mod)[1] + coef(quad.mod)[2]*x + coef(quad.mod)[3]*x*x}
# add to plot
plot (quad.fun, min(x), max(x), add = TRUE, lwd = 3, lty = 3, col = "forest green")

	}

    })
}

)

